Summary: Bash script to kickstart dssp5-fedora QEMU guests
Name: dssp5-fedora-kickstartqemu
Version: 0.1
Release: 1
License: Unlicense
Group: System Environment/Tools
Source: dssp5-fedora-kickstartqemu.tgz
URL: https://github.com/DefenSec/dssp5-fedora-kickstartqemu
Requires: qemu-img qemu-kvm-core curl
BuildRequires: argbash make python3-docutils
BuildArch: noarch

%description
Bash script to kickstart dssp5-fedora QEMU guests.

%prep
%autosetup -n dssp5-fedora-kickstartqemu

%build

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%license LICENSE
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.gz
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/%{name}

%changelog
* Sat May 08 2021 Dominick Grift <dominick.grift@defensec.nl> - 0.1-1
- Initial package
