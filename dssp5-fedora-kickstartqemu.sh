# SPDX-FileCopyrightText: © 2021 Dominick Grift <dominick.grift@defensec.nl>
# SPDX-License-Identifier: Unlicense

#!/usr/bin/env bash

# Created by argbash-init v2.9.0
# Rearrange the order of options below according to what you would like to see in the help message.
# ARG_OPTIONAL_SINGLE([name],[a],[Name for dssp5-fedora QEMU guest],[myguest])
# ARG_OPTIONAL_SINGLE([mem],[b],[Memory to allocate for dssp5-fedora QEMU guest in GB],[4])
# ARG_OPTIONAL_SINGLE([cores],[c],[CPU cores to allocate for dssp5-fedora QEMU guest],[4])
# ARG_OPTIONAL_SINGLE([ovmfcode],[d],[Absolute path to OVMF_CODE.fd],[/usr/share/edk2/ovmf/OVMF_CODE.fd])
# ARG_OPTIONAL_SINGLE([ovmfvars],[e],[Absolute path to OVMF_VARS.fd],[/usr/share/edk2/ovmf/OVMF_VARS.fd])
# ARG_OPTIONAL_SINGLE([size],[f],[Image size for dssp5-fedora QEMU guest in GB],[6])
# ARG_OPTIONAL_SINGLE([bootiso],[g],[Absolute file:// or https:// path to boot.iso],[https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os/images/boot.iso])
# ARG_OPTIONAL_SINGLE([kernel],[i],[Absolute file:// or https:// path to vmlinuz],[https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os/isolinux/vmlinuz])
# ARG_OPTIONAL_SINGLE([initrd],[j],[Absolute file:// or https:// path to initrd.img],[https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os/isolinux/initrd.img])
# ARG_OPTIONAL_SINGLE([osmedia],[k],[Absolute https:// path to os media tree],[https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os])
# ARG_OPTIONAL_SINGLE([rootpw],[l],[Password for dssp5-fedora QEMU guest root],[abc123])
# ARG_OPTIONAL_SINGLE([wheelpw],[m],[Password for dssp5-fedora QEMU guest wheel],[abc123])
# ARG_OPTIONAL_SINGLE([wheelname],[n],[Identity for dssp5-fedora QEMU guest wheel],[kcinimod])
# ARG_OPTIONAL_SINGLE([statedir],[o],[Absolute path to QEMU state directory],[${HOME}/.qemu])
# ARG_OPTIONAL_SINGLE([telnetport],[p],[Optional port to use with telnet])
# ARG_OPTIONAL_BOOLEAN([system],[q],[Install dssp5-fedora QEMU guest system-wide])
# ARGBASH_SET_INDENT([    ])
# ARGBASH_SET_DELIM([ =])
# ARG_OPTION_STACKING([getopt])
# ARG_RESTRICT_VALUES([no-local-options])
# ARG_VERSION_AUTO([1.0.0])
# ARG_VERBOSE([])
# ARG_HELP([Kickstart QEMU virtual machines],[I use this to automatically install Fedora virtual machines with MYDSSP3])
# ARGBASH_GO()
# needed because of Argbash --> m4_ignore([
### START OF CODE GENERATED BY Argbash v2.10.0 one line above ###
# Argbash is a bash code generator used to get arguments parsing right.
# Argbash is FREE SOFTWARE, see https://argbash.io for more info


die()
{
    local _ret="${2:-1}"
    test "${_PRINT_HELP:-no}" = yes && print_help >&2
    echo "$1" >&2
    exit "${_ret}"
}


evaluate_strictness()
{
    [[ "$2" =~ ^-(-(name|mem|cores|ovmfcode|ovmfvars|size|bootiso|kernel|initrd|osmedia|rootpw|wheelpw|wheelname|statedir|telnetport|system|version|verbose|help)$|[abcdefgijklmnopqvh]) ]] && die "You have passed '$2' as a value of argument '$1', which makes it look like that you have omitted the actual value, since '$2' is an option accepted by this script. This is considered a fatal error."
}


begins_with_short_option()
{
    local first_option all_short_options='abcdefgijklmnopqvh'
    first_option="${1:0:1}"
    test "$all_short_options" = "${all_short_options/$first_option/}" && return 1 || return 0
}

# THE DEFAULTS INITIALIZATION - OPTIONALS
_arg_name="myguest"
_arg_mem="4"
_arg_cores="4"
_arg_ovmfcode="/usr/share/edk2/ovmf/OVMF_CODE.fd"
_arg_ovmfvars="/usr/share/edk2/ovmf/OVMF_VARS.fd"
_arg_size="6"
_arg_bootiso="https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os/images/boot.iso"
_arg_kernel="https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os/isolinux/vmlinuz"
_arg_initrd="https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os/isolinux/initrd.img"
_arg_osmedia="https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os"
_arg_rootpw="abc123"
_arg_wheelpw="abc123"
_arg_wheelname="kcinimod"
_arg_statedir="${HOME}/.qemu"
_arg_telnetport=
_arg_system="off"
_arg_verbose=0


print_help()
{
    printf '%s\n' "Kickstart QEMU virtual machines"
    printf 'Usage: %s [-a|--name <arg>] [-b|--mem <arg>] [-c|--cores <arg>] [-d|--ovmfcode <arg>] [-e|--ovmfvars <arg>] [-f|--size <arg>] [-g|--bootiso <arg>] [-i|--kernel <arg>] [-j|--initrd <arg>] [-k|--osmedia <arg>] [-l|--rootpw <arg>] [-m|--wheelpw <arg>] [-n|--wheelname <arg>] [-o|--statedir <arg>] [-p|--telnetport <arg>] [-q|--(no-)system] [-v|--version] [--verbose] [-h|--help]\n' "$0"
    printf '\t%s\n' "-a, --name: Name for dssp5-fedora QEMU guest (default: 'myguest')"
    printf '\t%s\n' "-b, --mem: Memory to allocate for dssp5-fedora QEMU guest in GB (default: '4')"
    printf '\t%s\n' "-c, --cores: CPU cores to allocate for dssp5-fedora QEMU guest (default: '4')"
    printf '\t%s\n' "-d, --ovmfcode: Absolute path to OVMF_CODE.fd (default: '/usr/share/edk2/ovmf/OVMF_CODE.fd')"
    printf '\t%s\n' "-e, --ovmfvars: Absolute path to OVMF_VARS.fd (default: '/usr/share/edk2/ovmf/OVMF_VARS.fd')"
    printf '\t%s\n' "-f, --size: Image size for dssp5-fedora QEMU guest in GB (default: '6')"
    printf '\t%s\n' "-g, --bootiso: Absolute file:// or https:// path to boot.iso (default: 'https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os/images/boot.iso')"
    printf '\t%s\n' "-i, --kernel: Absolute file:// or https:// path to vmlinuz (default: 'https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os/isolinux/vmlinuz')"
    printf '\t%s\n' "-j, --initrd: Absolute file:// or https:// path to initrd.img (default: 'https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os/isolinux/initrd.img')"
    printf '\t%s\n' "-k, --osmedia: Absolute https:// path to os media tree (default: 'https://ams.edge.kernel.org/fedora-buffet/fedora/linux/development/rawhide/Everything/x86_64/os')"
    printf '\t%s\n' "-l, --rootpw: Password for dssp5-fedora QEMU guest root (default: 'abc123')"
    printf '\t%s\n' "-m, --wheelpw: Password for dssp5-fedora QEMU guest wheel (default: 'abc123')"
    printf '\t%s\n' "-n, --wheelname: Identity for dssp5-fedora QEMU guest wheel (default: 'kcinimod')"
    printf '\t%s\n' "-o, --statedir: Absolute path to QEMU state directory (default: '${HOME}/.qemu')"
    printf '\t%s\n' "-p, --telnetport: Optional port to use with telnet (no default)"
    printf '\t%s\n' "-q, --system, --no-system: Install dssp5-fedora QEMU guest system-wide (off by default)"
    printf '\t%s\n' "-v, --version: Prints version"
    printf '\t%s\n' "--verbose: Set verbose output (can be specified multiple times to increase the effect)"
    printf '\t%s\n' "-h, --help: Prints help"
    printf '\n%s\n' "I use this to automatically install Fedora virtual machines with MYDSSP3"
}


parse_commandline()
{
    while test $# -gt 0
    do
        _key="$1"
        case "$_key" in
            -a|--name)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_name="$2"
                shift
                evaluate_strictness "$_key" "$_arg_name"
                ;;
            --name=*)
                _arg_name="${_key##--name=}"
                evaluate_strictness "$_key" "$_arg_name"
                ;;
            -a*)
                _arg_name="${_key##-a}"
                evaluate_strictness "$_key" "$_arg_name"
                ;;
            -b|--mem)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_mem="$2"
                shift
                evaluate_strictness "$_key" "$_arg_mem"
                ;;
            --mem=*)
                _arg_mem="${_key##--mem=}"
                evaluate_strictness "$_key" "$_arg_mem"
                ;;
            -b*)
                _arg_mem="${_key##-b}"
                evaluate_strictness "$_key" "$_arg_mem"
                ;;
            -c|--cores)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_cores="$2"
                shift
                evaluate_strictness "$_key" "$_arg_cores"
                ;;
            --cores=*)
                _arg_cores="${_key##--cores=}"
                evaluate_strictness "$_key" "$_arg_cores"
                ;;
            -c*)
                _arg_cores="${_key##-c}"
                evaluate_strictness "$_key" "$_arg_cores"
                ;;
            -d|--ovmfcode)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_ovmfcode="$2"
                shift
                evaluate_strictness "$_key" "$_arg_ovmfcode"
                ;;
            --ovmfcode=*)
                _arg_ovmfcode="${_key##--ovmfcode=}"
                evaluate_strictness "$_key" "$_arg_ovmfcode"
                ;;
            -d*)
                _arg_ovmfcode="${_key##-d}"
                evaluate_strictness "$_key" "$_arg_ovmfcode"
                ;;
            -e|--ovmfvars)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_ovmfvars="$2"
                shift
                evaluate_strictness "$_key" "$_arg_ovmfvars"
                ;;
            --ovmfvars=*)
                _arg_ovmfvars="${_key##--ovmfvars=}"
                evaluate_strictness "$_key" "$_arg_ovmfvars"
                ;;
            -e*)
                _arg_ovmfvars="${_key##-e}"
                evaluate_strictness "$_key" "$_arg_ovmfvars"
                ;;
            -f|--size)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_size="$2"
                shift
                evaluate_strictness "$_key" "$_arg_size"
                ;;
            --size=*)
                _arg_size="${_key##--size=}"
                evaluate_strictness "$_key" "$_arg_size"
                ;;
            -f*)
                _arg_size="${_key##-f}"
                evaluate_strictness "$_key" "$_arg_size"
                ;;
            -g|--bootiso)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_bootiso="$2"
                shift
                evaluate_strictness "$_key" "$_arg_bootiso"
                ;;
            --bootiso=*)
                _arg_bootiso="${_key##--bootiso=}"
                evaluate_strictness "$_key" "$_arg_bootiso"
                ;;
            -g*)
                _arg_bootiso="${_key##-g}"
                evaluate_strictness "$_key" "$_arg_bootiso"
                ;;
            -i|--kernel)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_kernel="$2"
                shift
                evaluate_strictness "$_key" "$_arg_kernel"
                ;;
            --kernel=*)
                _arg_kernel="${_key##--kernel=}"
                evaluate_strictness "$_key" "$_arg_kernel"
                ;;
            -i*)
                _arg_kernel="${_key##-i}"
                evaluate_strictness "$_key" "$_arg_kernel"
                ;;
            -j|--initrd)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_initrd="$2"
                shift
                evaluate_strictness "$_key" "$_arg_initrd"
                ;;
            --initrd=*)
                _arg_initrd="${_key##--initrd=}"
                evaluate_strictness "$_key" "$_arg_initrd"
                ;;
            -j*)
                _arg_initrd="${_key##-j}"
                evaluate_strictness "$_key" "$_arg_initrd"
                ;;
            -k|--osmedia)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_osmedia="$2"
                shift
                evaluate_strictness "$_key" "$_arg_osmedia"
                ;;
            --osmedia=*)
                _arg_osmedia="${_key##--osmedia=}"
                evaluate_strictness "$_key" "$_arg_osmedia"
                ;;
            -k*)
                _arg_osmedia="${_key##-k}"
                evaluate_strictness "$_key" "$_arg_osmedia"
                ;;
            -l|--rootpw)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_rootpw="$2"
                shift
                evaluate_strictness "$_key" "$_arg_rootpw"
                ;;
            --rootpw=*)
                _arg_rootpw="${_key##--rootpw=}"
                evaluate_strictness "$_key" "$_arg_rootpw"
                ;;
            -l*)
                _arg_rootpw="${_key##-l}"
                evaluate_strictness "$_key" "$_arg_rootpw"
                ;;
            -m|--wheelpw)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_wheelpw="$2"
                shift
                evaluate_strictness "$_key" "$_arg_wheelpw"
                ;;
            --wheelpw=*)
                _arg_wheelpw="${_key##--wheelpw=}"
                evaluate_strictness "$_key" "$_arg_wheelpw"
                ;;
            -m*)
                _arg_wheelpw="${_key##-m}"
                evaluate_strictness "$_key" "$_arg_wheelpw"
                ;;
            -n|--wheelname)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_wheelname="$2"
                shift
                evaluate_strictness "$_key" "$_arg_wheelname"
                ;;
            --wheelname=*)
                _arg_wheelname="${_key##--wheelname=}"
                evaluate_strictness "$_key" "$_arg_wheelname"
                ;;
            -n*)
                _arg_wheelname="${_key##-n}"
                evaluate_strictness "$_key" "$_arg_wheelname"
                ;;
            -o|--statedir)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_statedir="$2"
                shift
                evaluate_strictness "$_key" "$_arg_statedir"
                ;;
            --statedir=*)
                _arg_statedir="${_key##--statedir=}"
                evaluate_strictness "$_key" "$_arg_statedir"
                ;;
            -o*)
                _arg_statedir="${_key##-o}"
                evaluate_strictness "$_key" "$_arg_statedir"
                ;;
            -p|--telnetport)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_telnetport="$2"
                shift
                evaluate_strictness "$_key" "$_arg_telnetport"
                ;;
            --telnetport=*)
                _arg_telnetport="${_key##--telnetport=}"
                evaluate_strictness "$_key" "$_arg_telnetport"
                ;;
            -p*)
                _arg_telnetport="${_key##-p}"
                evaluate_strictness "$_key" "$_arg_telnetport"
                ;;
            -q|--no-system|--system)
                _arg_system="on"
                test "${1:0:5}" = "--no-" && _arg_system="off"
                ;;
            -q*)
                _arg_system="on"
                _next="${_key##-q}"
                if test -n "$_next" -a "$_next" != "$_key"
                then
                    { begins_with_short_option "$_next" && shift && set -- "-q" "-${_next}" "$@"; } || die "The short option '$_key' can't be decomposed to ${_key:0:2} and -${_key:2}, because ${_key:0:2} doesn't accept value and '-${_key:2:1}' doesn't correspond to a short option."
                fi
                ;;
            -v|--version)
                printf '%s %s\n\n%s\n' "dssp5-fedora-kickstartqemu.sh" "1.0.0" ''
                exit 0
                ;;
            -v*)
                printf '%s %s\n\n%s\n' "dssp5-fedora-kickstartqemu.sh" "1.0.0" ''
                exit 0
                ;;
            --verbose)
                _arg_verbose=$((_arg_verbose + 1))
                ;;
            -h|--help)
                print_help
                exit 0
                ;;
            -h*)
                print_help
                exit 0
                ;;
            *)
                _PRINT_HELP=yes die "FATAL ERROR: Got an unexpected argument '$1'" 1
                ;;
        esac
        shift
    done
}

parse_commandline "$@"

# OTHER STUFF GENERATED BY Argbash

### END OF CODE GENERATED BY Argbash (sortof) ### ])
# [ <-- needed because of Argbash

test -e /usr/bin/qemu-system-x86_64 && test -e /usr/bin/qemu-kvm && \
    test -e /usr/bin/qemu-img || \
	die "qemu-img and qemu-kvm-core are required"

test -e $_arg_ovmfcode && test -e $_arg_ovmfvars || die "OVMF not found"

test -z "$(echo "$_arg_osmedia" | awk -F'https://' '{ print $1 }')" || \
    die "invalid os media tree"

if [[ $_arg_system == "on" ]]
then

    test "$_arg_statedir" == "${HOME}/.qemu" && \
	die "invalid system-wide state directory"

    test -e ${_arg_statedir}/${_arg_name} && die "guest exists"

    if [[ -z "$(echo "$_arg_bootiso" | awk -F'file://' '{ print $1 }')" ]]
    then

	test -e "$(echo "$_arg_bootiso" | awk -F'file://' '{ print $2 }')" && \
	    export _arg_bootiso_file=$(echo "$_arg_bootiso" | awk -F'file://' '{ print $2 }') || \
	    die "invalid boot iso"

    elif [[ -z $_arg_bootiso_file ]]
    then

	test -z "$(echo "$_arg_bootiso" | awk -F'https://' '{ print $1 }')" && \
	    mkdir -p ${_arg_statedir}/${_arg_name} && \
	    curl --silent -o ${_arg_statedir}/${_arg_name}/boot.iso $_arg_bootiso && \
	    export _arg_bootiso_file=${_arg_statedir}/${_arg_name}/boot.iso || \
		die "invalid boot iso"

    fi

    if [[ -z "$(echo "$_arg_kernel" | awk -F'file://' '{ print $1 }')" ]]
    then

	test -e "$(echo "$_arg_kernel" | awk -F'file://' '{ print $2 }')" && \
	    export _arg_kernel_file=$(echo "$_arg_kernel" | awk -F'file://' '{ print $2 }') || \
		die "invalid kernel"

    elif [[ -z "$_arg_kernel_file" ]]
    then

	test -z "$(echo "$_arg_kernel" | awk -F'https://' '{ print $1 }')" && \
	    mkdir -p ${_arg_statedir}/${_arg_name} && \
	    curl --silent -o ${_arg_statedir}/${_arg_name}/vmlinuz $_arg_kernel && \
	    export _arg_kernel_file=${_arg_statedir}/${_arg_name}/vmlinuz || \
		die "invalid kernel"

    fi

    if [[ -z "$(echo "$_arg_initrd" | awk -F'file://' '{ print $1 }')" ]]
    then

	test -e "$(echo "$_arg_initrd" | awk -F'file://' '{ print $2 }')" && \
	    export _arg_initrd_file=$(echo "$_arg_initrd" | awk -F'file://' '{ print $2 }') || \
		die "invalid initrd"

    elif [[ -z "$_arg_initrd_file" ]]
    then

	test -z "$(echo "$_arg_initrd" | awk -F'https://' '{ print $1 }')" && \
	    mkdir -p ${_arg_statedir}/${_arg_name} && \
	    curl --silent -o ${_arg_statedir}/${_arg_name}/initrd.img $_arg_initrd && \
	    export _arg_initrd_file=${_arg_statedir}/${_arg_name}/initrd.img || \
		die "invalid initrd"

    fi

    mkdir -p ${_arg_statedir}/${_arg_name} && \
	qemu-img create -f qcow2 -o size=${_arg_size}G \
		 ${_arg_statedir}/${_arg_name}/${_arg_name}.img && \
	chcon -u sys.id ${_arg_statedir}/${_arg_name}/${_arg_name}.img && \
	chown root.qemu ${_arg_statedir}/${_arg_name}/${_arg_name}.img && \
	chmod 0664 ${_arg_statedir}/${_arg_name}/${_arg_name}.img || \
	    die "qemu-img failed"

    mkdir -p ${_arg_statedir}/${_arg_name} && \
	cp $_arg_ovmfvars ${_arg_statedir}/${_arg_name}/ && \
	chcon -u sys.id ${_arg_statedir}/${_arg_name}/OVMF_VARS.fd && \
	chown root.qemu ${_arg_statedir}/${_arg_name}/OVMF_VARS.fd && \
	chmod 0664 ${_arg_statedir}/${_arg_name}/OVMF_VARS.fd || \
	    die "ovmfvars failed"

    _arg_ks=$(cat <<EOF |
autopart --type=btrfs --noswap
bootloader --append=$(printf "%s\n" "\"enforcing=1 console=tty0 console=ttyS0,115200n8 serial rd_NO_PLYMOUTH\"")
clearpart --all --initlabel --disklabel=msdos
keyboard --vckeymap=us-euro --xlayouts=us
lang en_US.UTF-8
network --bootproto=dhcp --device=link --hostname=${_arg_name} --activate
rootpw $_arg_rootpw
shutdown
text
timezone Europe/Amsterdam --utc
url --url=${_arg_osmedia}
user --groups=wheel --name=${_arg_wheelname} --password=${_arg_wheelpw}
zerombr

repo --name=updates
repo --name=dssp5 --baseurl=https://dgrift.home.xs4all.nl/dssp5/

%packages --excludedocs --instLangs=en --ignoremissing --nocore --exclude-weakdeps
authselect
basesystem
bash
bash-completion
binutils
coreutils
cracklib-dicts
dnf
dnf-plugins-core
dssp5-fedora
dssp5-repos
fedora-release
filesystem
glibc
kbd
kernel
kitty-terminfo
langpacks-en
less
ncurses
openssh-server
passwd
python3-libselinux
qemu-guest-agent
rootfiles
rpm-plugin-selinux
secilc
setools-console
setup
shadow-utils
sudo
systemd-networkd
systemd-container
systemd-udev
util-linux
vim-minimal
zram-generator-defaults
-chrony
%end

%post
systemctl disable dnf-makecache.timer systemd-homed
systemctl enable systemd-resolved systemd-networkd systemd-timesyncd
ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf

cat > /etc/systemd/network/enp0s3.network <<FOE
[Match]
Name=enp0s3
[Network]
DHCP=yes
FOE

cat > /etc/selinux/semanage.conf <<EFO
module-store = direct
expand-check = 1
usepasswd = true
ignoredirs=/root;/var/lib/gdm;/run/gnome-initial-setup
[sefcontext_compile]
path = /usr/sbin/sefcontext_compile
args = -r \\\$@
[end]
EFO

echo "%gitshell.id" >> /etc/security/sepermit.conf
echo "%user.id" >> /etc/security/sepermit.conf
echo "%wheel.id" >> /etc/security/sepermit.conf

echo "install_weak_deps=0" >> /etc/dnf/dnf.conf
echo "tsflags=nodocs" >> /etc/dnf/dnf.conf

sed -i 's/#Storage=auto/Storage=volatile/' /etc/systemd/journald.conf
sed -i 's/#ReadKMsg=yes/ReadKMsg=no/' /etc/systemd/journald.conf
sed -i 's/#MaxLevelStore=debug/MaxLevelStore=info/' /etc/systemd/journald.conf
sed -i 's/#MaxLevelSyslog=debug/MaxLevelSyslog=info/' /etc/systemd/journald.conf

authselect select minimal -f

semodule -BNP
restorecon -RF /

mkdir /relabel
mount --bind / /relabel
chcon -u sys.id -r sys.role -t sys.fs -l s0 /relabel/sys
chcon -u sys.id -r sys.role -t run.file -l s0 /relabel/run
chcon -u sys.id -r sys.role -t proc.fs -l s0 /relabel/proc
chcon -u sys.id -r sys.role -t dev.file -l s0 /relabel/dev
chcon -u sys.id -r sys.role -t boot.file -l s0 /relabel/boot
chcon -u sys.id -r sys.role -t tmp.file -l s0 /relabel/tmp
chcon -u sys.id -r sys.role -t home.file -l s0 /relabel/home
rm -f -- /relabel/tmp/ks-script-*
umount /relabel
mount --bind /boot /relabel
chcon -u sys.id -r sys.role -t dos.fs -l s0 /relabel/efi
umount /relabel
mount /dev/mapper/$(dmsetup ls | awk '{ print \$1 }') /relabel
chcon -u sys.id -r sys.role -t root.file -l s0 /relabel
umount /relabel
rmdir /relabel

mount -t tmpfs none /tmp
mount -t tmpfs none /root
mount -t tmpfs none /var/log
%end
EOF
	      curl --silent -F 'sprunge=<-' http://sprunge.us || \
		  die "kickstart failed")

    if [[ -n "$_arg_telnetport" ]]
    then

	qemu-system-x86_64 \
	    -enable-kvm -boot menu=on \
	    -drive \
	    file=${_arg_ovmfcode},if=pflash,format=raw,unit=0,readonly=on \
	    -drive \
	    file=${_arg_statedir}/${_arg_name}/OVMF_VARS.fd,if=pflash,format=raw,unit=1 \
	    -drive \
	    id=disk0,file=${_arg_statedir}/${_arg_name}/${_arg_name}.img,format=qcow2,if=none \
	    -device virtio-blk-pci,drive=disk0,bootindex=1 \
	    -drive \
	    id=cd0,if=none,format=raw,readonly,file=${_arg_bootiso_file} \
	    -device ide-cd,bus=ide.1,drive=cd0,bootindex=2 \
	    -global PIIX4_PM.disable_s3=0 \
	    -rtc clock=host,base=localtime \
	    -vga none -m ${_arg_mem}G \
	    -netdev id=net0,type=user \
	    -device virtio-net-pci,netdev=net0,romfile= \
	    -name $_arg_name -cpu host -no-reboot \
	    -machine vmport=off \
	    -smp cores=${_arg_cores},maxcpus=${_arg_cores} \
	    -serial telnet:localhost:${_arg_telnetport},server,nowait \
	    -kernel $_arg_kernel_file \
	    -initrd $_arg_initrd_file \
	    -append \
	    "@ROOT@ console=tty0 console=ttyS0,115200 serial rd_NO_PLYMOUTH inst.ks=${_arg_ks}" \
	    -display none -daemonize || die "qemu failed"

    elif [[ -z "$_arg_telnetport" ]]
    then

	qemu-system-x86_64 \
	    -enable-kvm -boot menu=on \
	    -drive \
	    file=${_arg_ovmfcode},if=pflash,format=raw,unit=0,readonly=on \
	    -drive \
	    file=${_arg_statedir}/${_arg_name}/OVMF_VARS.fd,if=pflash,format=raw,unit=1 \
	    -drive \
	    id=disk0,file=${_arg_statedir}/${_arg_name}/${_arg_name}.img,format=qcow2,if=none \
	    -device virtio-blk-pci,drive=disk0,bootindex=1 \
	    -drive \
	    id=cd0,if=none,format=raw,readonly,file=${_arg_bootiso_file} \
	    -device ide-cd,bus=ide.1,drive=cd0,bootindex=2 \
	    -global PIIX4_PM.disable_s3=0 \
	    -rtc clock=host,base=localtime \
	    -vga none -m ${_arg_mem}G \
	    -netdev id=net0,type=user \
	    -device virtio-net-pci,netdev=net0,romfile= \
	    -name $_arg_name -cpu host -no-reboot \
	    -machine vmport=off \
	    -smp cores=${_arg_cores},maxcpus=${_arg_cores} \
	    -kernel $_arg_kernel_file \
	    -initrd $_arg_initrd_file \
	    -append \
	    "@ROOT@ console=tty0 console=ttyS0,115200 serial rd_NO_PLYMOUTH inst.ks=${_arg_ks}" \
	    -display none -daemonize || \
	    die "qemu failed"

    fi

elif [[ $_arg_system == "off" ]]
then

    test -e ${_arg_statedir}/${_arg_name} && \
	die "guest exists"

    if [[ -z "$(echo "$_arg_bootiso" | awk -F'file://' '{ print $1 }')" ]]
    then

	test -e "$(echo "$_arg_bootiso" | awk -F'file://' '{ print $2 }')" && \
	    export _arg_bootiso_file=$(echo "$_arg_bootiso" | awk -F'file://' '{ print $2 }') || \
		die "invalid boot iso"

    elif [[ -z "$_arg_bootiso_file" ]]
    then

	test -z "$(echo "$_arg_bootiso" | awk -F'https://' '{ print $1 }')" && \
	    mkdir -p ${HOME}/Downloads/${_arg_name} && \
	    curl --silent -o ${HOME}/Downloads/${_arg_name}/boot.iso $_arg_bootiso && \
	    export _arg_bootiso_file=${HOME}/Downloads/${_arg_name}/boot.iso || \
		die "invalid boot iso"

    fi

    if [[ -z "$(echo "$_arg_kernel" | awk -F'file://' '{ print $1 }')" ]]
    then

	test -e "$(echo "$_arg_kernel" | awk -F'file://' '{ print $2 }')" && \
	    export _arg_kernel_file=$(echo "$_arg_kernel" | awk -F'file://' '{ print $2 }') || \
		die "invalid kernel"

    elif [[ -z "$_arg_kernel_file" ]]
    then

	test -z "$(echo "$_arg_kernel" | awk -F'https://' '{ print $1 }')" && \
	    mkdir -p ${HOME}/Downloads/${_arg_name} && \
	    curl --silent -o ${HOME}/Downloads/${_arg_name}/vmlinuz $_arg_kernel && \
	    export _arg_kernel_file=${HOME}/Downloads/${_arg_name}/vmlinuz || \
		die "invalid kernel"

    fi

    if [[ -z "$(echo "$_arg_initrd" | awk -F'file://' '{ print $1 }')" ]]
    then

	test -e "$(echo "$_arg_initrd" | awk -F'file://' '{ print $2 }')" && \
	    export _arg_initrd_file=$(echo "$_arg_initrd" | awk -F'file://' '{ print $2 }') || \
		die "invalid initrd"

    elif [[ -z "$_arg_initrd_file" ]]
    then

	test -z "$(echo "$_arg_initrd" | awk -F'https://' '{ print $1 }')" && \
	    mkdir -p ${HOME}/Downloads/${_arg_name} && \
	    curl --silent -o ${HOME}/Downloads/${_arg_name}/initrd.img $_arg_initrd && \
	    export _arg_initrd_file=${HOME}/Downloads/${_arg_name}/initrd.img || \
		die "invalid initrd"

    fi

    mkdir -p ${_arg_statedir}/${_arg_name} && \
	qemu-img create -f qcow2 \
		 -o size=${_arg_size}G ${_arg_statedir}/${_arg_name}/${_arg_name}.img || \
	    die "qemu-img failed"

    mkdir -p ${_arg_statedir}/${_arg_name} && \
	cp $_arg_ovmfvars ${_arg_statedir}/${_arg_name}/ || \
	    die "ovmfvars failed"

    _arg_ks=$(cat <<EOF |
autopart --type=btrfs --noswap
bootloader --append=$(printf "%s\n" "\"enforcing=1 console=tty0 console=ttyS0,115200n8 serial rd_NO_PLYMOUTH\"")
clearpart --all --initlabel --disklabel=msdos
keyboard --vckeymap=us-euro --xlayouts=us
lang en_US.UTF-8
network --bootproto=dhcp --device=link --hostname=${_arg_name} --activate
rootpw $_arg_rootpw
shutdown
text
timezone Europe/Amsterdam --utc
url --url=${_arg_osmedia}
user --groups=wheel --name=${_arg_wheelname} --password=${_arg_wheelpw}
zerombr

repo --name=updates
repo --name=dssp5 --baseurl=https://dgrift.home.xs4all.nl/dssp5/

%packages --excludedocs --instLangs=en --ignoremissing --nocore --exclude-weakdeps
authselect
basesystem
bash
bash-completion
binutils
coreutils
cracklib-dicts
dnf
dnf-plugins-core
dssp5-fedora
dssp5-repos
fedora-release
filesystem
glibc
kbd
kernel
kitty-terminfo
langpacks-en
less
ncurses
openssh-server
passwd
python3-libselinux
qemu-guest-agent
rootfiles
rpm-plugin-selinux
secilc
setools-console
setup
shadow-utils
sudo
systemd-networkd
systemd-container
systemd-udev
util-linux
vim-minimal
zram-generator-defaults
-chrony
%end

%post
systemctl disable dnf-makecache.timer systemd-homed
systemctl enable systemd-resolved systemd-networkd systemd-timesyncd
ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf

cat > /etc/systemd/network/enp0s3.network <<FOE
[Match]
Name=enp0s3
[Network]
DHCP=yes
FOE

cat > /etc/selinux/semanage.conf <<EFO
module-store = direct
expand-check = 1
usepasswd = true
ignoredirs=/root;/var/lib/gdm;/run/gnome-initial-setup
[sefcontext_compile]
path = /usr/sbin/sefcontext_compile
args = -r \\\$@
[end]
EFO

echo "%gitshell.id" >> /etc/security/sepermit.conf
echo "%user.id" >> /etc/security/sepermit.conf
echo "%wheel.id" >> /etc/security/sepermit.conf

echo "install_weak_deps=0" >> /etc/dnf/dnf.conf
echo "tsflags=nodocs" >> /etc/dnf/dnf.conf

sed -i 's/#Storage=auto/Storage=volatile/' /etc/systemd/journald.conf
sed -i 's/#ReadKMsg=yes/ReadKMsg=no/' /etc/systemd/journald.conf
sed -i 's/#MaxLevelStore=debug/MaxLevelStore=info/' /etc/systemd/journald.conf
sed -i 's/#MaxLevelSyslog=debug/MaxLevelSyslog=info/' /etc/systemd/journald.conf

authselect select minimal -f

semodule -BNP
restorecon -RF /

mkdir /relabel
mount --bind / /relabel
chcon -u sys.id -r sys.role -t sys.fs -l s0 /relabel/sys
chcon -u sys.id -r sys.role -t run.file -l s0 /relabel/run
chcon -u sys.id -r sys.role -t proc.fs -l s0 /relabel/proc
chcon -u sys.id -r sys.role -t dev.file -l s0 /relabel/dev
chcon -u sys.id -r sys.role -t boot.file -l s0 /relabel/boot
chcon -u sys.id -r sys.role -t tmp.file -l s0 /relabel/tmp
chcon -u sys.id -r sys.role -t home.file -l s0 /relabel/home
rm -f -- /relabel/tmp/ks-script-*
umount /relabel
mount --bind /boot /relabel
chcon -u sys.id -r sys.role -t dos.fs -l s0 /relabel/efi
umount /relabel
mount /dev/mapper/$(dmsetup ls | awk '{ print \$1 }') /relabel
chcon -u sys.id -r sys.role -t root.file -l s0 /relabel
umount /relabel
rmdir /relabel

mount -t tmpfs none /tmp
mount -t tmpfs none /root
mount -t tmpfs none /var/log
%end
EOF
	      curl --silent -F 'sprunge=<-' http://sprunge.us || \
		  die "kickstart failed")

    if [[ -n "$_arg_telnetport" ]]
    then

	qemu-system-x86_64 \
	    -enable-kvm -boot menu=on \
	    -drive \
	    file=${_arg_ovmfcode},if=pflash,format=raw,unit=0,readonly=on \
	    -drive \
	    file=${_arg_statedir}/${_arg_name}/OVMF_VARS.fd,if=pflash,format=raw,unit=1 \
	    -drive \
	    id=disk0,file=${_arg_statedir}/${_arg_name}/${_arg_name}.img,format=qcow2,if=none \
	    -device virtio-blk-pci,drive=disk0,bootindex=1 \
	    -drive \
	    id=cd0,if=none,format=raw,readonly,file=${_arg_bootiso_file} \
	    -device ide-cd,bus=ide.1,drive=cd0,bootindex=2 \
	    -global PIIX4_PM.disable_s3=0 \
	    -rtc clock=host,base=localtime \
	    -vga virtio -m ${_arg_mem}G \
	    -netdev id=net0,type=user \
	    -device virtio-net-pci,netdev=net0,romfile= \
	    -name $_arg_name -cpu host -no-reboot \
	    -machine vmport=off \
	    -smp cores=${_arg_cores},maxcpus=${_arg_cores} \
	    -serial telnet:localhost:${_arg_telnetport},server,nowait \
	    -kernel $_arg_kernel_file \
	    -initrd $_arg_initrd_file \
	    -append \
	    "@ROOT@ console=tty0 console=ttyS0,115200 serial rd_NO_PLYMOUTH inst.ks=${_arg_ks}" \
	    -display none -daemonize || die "qemu failed"

    elif [[ -z "$_arg_telnetport" ]]
    then

	qemu-system-x86_64 \
	    -enable-kvm -boot menu=on \
	    -drive \
	    file=${_arg_ovmfcode},if=pflash,format=raw,unit=0,readonly=on \
	    -drive \
	    file=${_arg_statedir}/${_arg_name}/OVMF_VARS.fd,if=pflash,format=raw,unit=1 \
	    -drive \
	    id=disk0,file=${_arg_statedir}/${_arg_name}/${_arg_name}.img,format=qcow2,if=none \
	    -device virtio-blk-pci,drive=disk0,bootindex=1 \
	    -drive \
	    id=cd0,if=none,format=raw,readonly,file=${_arg_bootiso_file} \
	    -device ide-cd,bus=ide.1,drive=cd0,bootindex=2 \
	    -global PIIX4_PM.disable_s3=0 \
	    -rtc clock=host,base=localtime \
	    -vga virtio -m ${_arg_mem}G \
	    -netdev id=net0,type=user \
	    -device virtio-net-pci,netdev=net0,romfile= \
	    -name $_arg_name -cpu host -no-reboot \
	    -machine vmport=off \
	    -smp cores=${_arg_cores},maxcpus=${_arg_cores} \
	    -kernel $_arg_kernel_file \
	    -initrd $_arg_initrd_file \
	    -append \
	    "@ROOT@ console=tty0 console=ttyS0,115200 serial rd_NO_PLYMOUTH inst.ks=${_arg_ks}" \
	    -display none -daemonize || die "qemu failed"

    fi

fi

#EOF
# ] <-- needed because of Argbash
